#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "main.h"
#include "comp.h"
#include "gui.h"
#include "prg_serial_nonblock.h"
#include "utils.h"

#define MESSAGE_SIZE (sizeof(message))

#ifndef BAUD_RATE
#define BAUD_RATE B230400
#endif

pthread_mutex_t mtx;
char *ser;

void *main_thread(void *d)
{
    int fd = *(int *)d;

    queue_init();
    comp_init();
    gui_init();

    bool quit = false;
    while (!quit)
    {

        event ev = queue_pop();

        if (ev.source == EV_KEYBOARD)
        {
            keyboard_events(ev, fd);
        }
        else if (ev.source == EV_NUCLEO)
        {
            nucleo_events(ev);
        }

        quit = is_quit();
    } // end main quit

    gui_clean();
    comp_clean();
    queue_cleanup();
    return NULL;
}

bool send_message(int fd, message *msg)
{
    int msg_size;
    uint8_t msg_buf[MESSAGE_SIZE];

    if (!fill_message_buf(msg, msg_buf, MESSAGE_SIZE, &msg_size))
    {
        return false;
    }

    write(fd, msg_buf, msg_size);
    return true;
}

void keyboard_events(event ev, int fd)
{
    message msg;
    msg.type = MSG_NBR;

    switch (ev.type)
    {
    case EV_GET_VERSION:
    { // prepare packet for get version
        msg.type = MSG_GET_VERSION;
        info("Get version requested");
    }
    break;

    case EV_RESET_CHUNK:
        if (is_paused())
        {
            info("chunk reseted");
            reset_chunk();
        }
        else
        {
            error("can't reset chunk while computing");
        }
        break;

    case EV_SET_COMPUTE:
        fprintf(stderr, "Select values using arrows\nConfirm changes using Enter\n");
        choose_compute();
        comp_clean();
        comp_init();
        if (set_compute(&msg))
        {
            info("set compute");
        }
        else
        {
            error("set compute failed");
        }
        break;

    case EV_COMPUTE:
        //fprintf(stderr, "INFO: compute\n\r");
        if (!is_move())
        {
            if (!compute(&msg))
            {
                fprintf(stderr, "WARN: New computation requested but it is discarded due on ongoing computation\n\r");
            }
        }
        else
        {
            if (!compute_move(is_move(), &msg))
            {
                fprintf(stderr, "WARN: New computation requested but it is discarded due on ongoing computation\n\r");
            }
        }
        break;

    case EV_ABORT:
        if (!is_paused())
        {
            msg.type = MSG_ABORT;
            info("pause from keyboard");
            abort_computing();
        }
        else
        {
            info("Abort requested but it is not computing");
        }
        break;

    case EV_CLEAR_BUFFER:
        comp_clean();
        gui_refresh();
        comp_init();
        info("buffer cleared");
        break;

    case EV_REFRESH:
        gui_refresh();
        info("gui refreshed");
        break;

    case EV_QUIT:
        if (!is_paused())
        {
            msg.type = MSG_ABORT;
            abort_computing();
        }
        break;

    case EV_COMP_COMP:
        if (!is_paused())
        {
            msg.type = MSG_ABORT;
            if (!send_message(fd, &msg))
            {
                error("can't send message");
            }
            msg.type = MSG_NBR;
            abort_computing();
        }
        info("compute on computer");
        compute_on_comp();
        gui_refresh();
        break;

    case EV_MOVE:
    if (ser != NULL){
        if (!is_computing())
        {
            move_grid(ev.data.param);

            ev.type = EV_COMPUTE;
            queue_push(ev);
        }
        else
        {
            error("can't move while computing");
        }
    } else {
        move_grid(ev.data.param);
        compute_on_comp();
        gui_refresh();
    }
    break;

    case EV_ZOOM_OUT:
        zoom_out();
        comp_clean();
        comp_init();
        compute_on_comp();
        gui_refresh();
        set_compute(&msg);
        break;

    case EV_ZOOM_IN:
        zoom_in();
        comp_clean();
        comp_init();
        compute_on_comp();
        gui_refresh();
        set_compute(&msg);
        break;

    default:
        break;
    }
    if (msg.type != MSG_NBR)
    { // messge has been set
        if (!send_message(fd, &msg))
        {
            error("send_message() does not send all bytes of the message!");
        }
    }
}

void nucleo_events(event ev)
{
    if (ev.type == EV_SERIAL)
    {
        message *msg = ev.data.msg;
        switch (msg->type)
        {
        case MSG_STARTUP:
        {
            info("Nucleo restarted");
        }
        break;

        case MSG_VERSION:
            if (msg->data.version.patch > 0)
            {
                fprintf(stderr, "\e[1;34mINFO:\e[0m Nucleo firmware ver. %d.%d-p%d\n", msg->data.version.major, msg->data.version.minor, msg->data.version.patch);
            }
            else
            {
                fprintf(stderr, "\e[1;34mINFO:\e[0m Nucleo firmware ver. %d.%d\n", msg->data.version.major, msg->data.version.minor);
            }
            break;

        case MSG_COMPUTE_DATA:
            if (is_computing())
            {
                update_grid(&(msg->data.compute_data));
            }
            else
            {
                //  Nucleo sends new data without computing
            }
            break;

        case MSG_COMPUTE_DATA_BURST:
            if (is_computing())
            {
                update_grid_burst(&(msg->data.compute_data_burst));
            }
            else
            {
                //  Nucleo sends new data without computing
            }
            break;

        case MSG_DONE:
            gui_refresh();
            if (is_done())
            {
                info("computation done");
            }
            else
            {
                event ev = {.source = EV_KEYBOARD, .type = EV_COMPUTE};
                queue_push(ev);
            }
            break;

        case MSG_ABORT:
            if (!is_paused())
            {
                info("Abort from Nucleo");
                abort_computing();
            }
            else
            {
                info("Abort requested but it is not computing");
            }
            break;

        case MSG_ERROR:
            error("Receive error from Nucleo");
            break;

        case MSG_OK:
            break;

        default:
            error("Unknown message type has been received");
            break;
        }
        if (msg)
        {
            free(msg);
            msg = NULL;
        }
    }
    else if (ev.type == EV_QUIT)
    {
        set_quit();
    }
    else
    {
        // ignore all other events
    }
}

void set_serial(char *serial)
{
    ser = serial;
}