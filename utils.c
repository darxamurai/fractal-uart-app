#include "utils.h"

void info(char* msg){
    fprintf(stderr, "\e[1;34mINFO:\e[0m %s\n", msg);
}

void error(char* msg){
    fprintf(stderr, "\e[1;31mINFO:\e[0m %s\n", msg);
}



double absolute(double re, double im)
{
    return sqrt(re * re + im * im);
}

void complex_square(double *re, double *im)
{
    double a = (*re) * (*re) - (*im) * (*im);
    double b = 2 * (*re) * (*im);

    *re = a;
    *im = b;
}



void call_termios(int reset)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset)
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
    else
    {
        tioOld = tio; //backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

void color_switch(int i, double c_re, double c_im, double re_min, double re_max, double im_min, double im_max){
    switch (i){
        case 0:
        fprintf(stderr, "\rconst: Re = \e[1;32m%.2f\e[0m Im = %.2f \t plane sizes: Re min = %.2f Re max = %.2f  Im min = %.2f Im max = %.2f", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        case 1:
        fprintf(stderr, "\rconst: Re = %.2f Im = \e[1;32m%.2f\e[0m \t plane sizes: Re min = %.2f Re max = %.2f  Im min = %.2f Im max = %.2f", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        case 2:
        fprintf(stderr, "\rconst: Re = %.2f Im = %.2f \t plane sizes: Re min = \e[1;32m%.2f\e[0m Re max = %.2f  Im min = %.2f Im max = %.2f", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        case 3:
        fprintf(stderr, "\rconst: Re = %.2f Im = %.2f \t plane sizes: Re min = %.2f Re max = \e[1;32m%.2f\e[0m  Im min = %.2f Im max = %.2f", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        case 4:
        fprintf(stderr, "\rconst: Re = %.2f Im = %.2f \t plane sizes: Re min = %.2f Re max = %.2f  Im min = \e[1;32m%.2f\e[0m Im max = %.2f", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        case 5:
        fprintf(stderr, "\rconst: Re = %.2f Im = %.2f \t plane sizes: Re min = %.2f Re max = %.2f  Im min = %.2f Im max = \e[1;32m%.2f\e[0m", c_re, c_im, re_min, re_max, im_min, im_max);
        break;
        default:
        break;
    }
}