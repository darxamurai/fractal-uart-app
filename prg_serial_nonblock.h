
#ifndef __PRG_SERIAL_NONBLOCK_H__
#define __PRG_SERIAL_NONBLOCK_H__

#include <termios.h>

/// ----------------------------------------------------------------------------
/// @brief serial_open
/// 
/// @param fname  -- device name
/// 
/// @return 
/// ----------------------------------------------------------------------------
int serial_open(const char *fname, speed_t baud_rate);

/// ----------------------------------------------------------------------------
/// @brief serial_close
/// 
/// @param fd 
/// 
/// @return 
/// ----------------------------------------------------------------------------
int serial_close(int fd);

/// ----------------------------------------------------------------------------
/// @brief serial_putc
/// 
/// @param fd 
/// @param c 
/// 
/// @return 
/// ----------------------------------------------------------------------------
int serial_putc(int fd, char c);

/// ----------------------------------------------------------------------------
/// @brief serial_getc
/// 
/// @param fd 
/// 
/// @return 
/// ----------------------------------------------------------------------------
int serial_getc(int fd);

/// ----------------------------------------------------------------------------
/// @brief serial_getc_timeout
/// 
/// @param fd 
/// 
/// @return -1 on error, 0 no byte ready within the timeout, 1 one byte read
/// ----------------------------------------------------------------------------
int serial_getc_timeout(int fd, int timeout_ms, unsigned char *c);

#endif
