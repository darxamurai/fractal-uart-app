#include "gui.h"
#include "event_queue.h"

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#define SDL_WAIT_MS 10

static struct
{
    int w;
    int h;
    unsigned char *img;
} gui = {.img = NULL};

void gui_init()
{
    get_img_size(&gui.w, &gui.h);
    gui.img = (unsigned char *)malloc(gui.w * gui.h * 3);
    xwin_init(gui.w, gui.h);
}

void gui_clean()
{
    if (gui.img)
    {
        free(gui.img);
        gui.img = NULL;
    }
    xwin_close();
}

void gui_refresh()
{
    if (gui.img)
    {
        update_image(gui.w, gui.h, gui.img);
        xwin_redraw(gui.w, gui.h, gui.img);
    }
}

void* gui_thread(void* d){
    bool quit = false;
    SDL_Event sdl_ev;
    event ev = {.source = EV_KEYBOARD};

    while (!quit)
    {
        ev.type = EV_TYPE_NUM;
    // fprintf(stderr, "gui1\n");

        if (SDL_PollEvent(&sdl_ev)){
            switch (sdl_ev.type){
                case SDL_KEYDOWN:
                switch (sdl_ev.key.keysym.sym){
                    case SDLK_q:
                    ev.type = EV_QUIT;
                    set_quit();
                    break;

                    case SDLK_s:
                    ev.type = EV_SET_COMPUTE;
                    break;

                    case SDLK_1:
                    ev.type = EV_COMPUTE;
                    ev.data.param = 0;
                    break;

                    case SDLK_a:
                    ev.type = EV_ABORT;
                    break;

                    case SDLK_g:
                    ev.type = EV_GET_VERSION;
                    break;

                    case SDLK_p:
                    ev.type = EV_REFRESH;
                    break;

                    case SDLK_r:
                    ev.type = EV_RESET_CHUNK;
                    break;

                    case SDLK_UP:
                    ev.type = EV_MOVE;
                    ev.data.param = 1;
                    break;

                    case SDLK_DOWN:
                    ev.type = EV_MOVE;
                    ev.data.param = 2;
                    break;

                    case SDLK_LEFT:
                    ev.type = EV_MOVE;
                    ev.data.param = 3;
                    break;

                    case SDLK_RIGHT:
                    ev.type = EV_MOVE;
                    ev.data.param = 4;
                    break;

                    case SDLK_c:
                    ev.type = EV_COMP_COMP;
                    break;

                    case SDLK_MINUS:
                    ev.type = EV_ZOOM_OUT;
                    break;

                    case SDLK_EQUALS:
                    ev.type = EV_ZOOM_IN;
                    break;

                    default:
                    break;
                }
                break;
                default:
                break;
            }
    // fprintf(stderr, "gui2\n");

            if (ev.type != EV_TYPE_NUM){
                queue_push(ev);
            }
        }

        SDL_Delay(SDL_WAIT_MS);
        quit = is_quit();
    }
    
    return NULL;
}