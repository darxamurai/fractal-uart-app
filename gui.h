#ifndef __GUI_H__
#define __GUI_H__

#include "xwin_sdl.h"
#include "comp.h"

void gui_init();
void gui_clean();

void gui_refresh();

void* gui_thread(void*);

#endif