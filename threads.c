#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h> 

#include "event_queue.h"
#include "threads.h"
#include "prg_serial_nonblock.h"
#include "comp.h"
#include "utils.h"

#define SERIAL_READ_TIMOUT_MS 500 //timeout for reading from serial port

void *keyboard_thread(void *d)
{
    fprintf(stderr, "keyboard\n");
    return 0;
    bool quit = false;
    int c;
    event ev = {.source = EV_KEYBOARD};

    call_termios(0);
    fprintf(stderr, "keyboard1\n");

    while (!quit && (c = getchar()))
    {
        ev.type = EV_TYPE_NUM;
        switch (c)
        {
        case 'g': // get version
            ev.type = EV_GET_VERSION;
            break;
        case 'q':
            set_quit();
            ev.type = EV_QUIT;
            break;
        case 'r':
            ev.type = EV_RESET_CHUNK;
            break;
        case 'a':
            ev.type = EV_ABORT;
            break;
        case 's':
            ev.type = EV_SET_COMPUTE;
            break;
        case '1':
            ev.type = EV_COMPUTE;
            break;
        case 'l':
            ev.type = EV_CLEAR_BUFFER;
            break;
        case 'p':
            ev.type = EV_REFRESH;
            break;
        case 'c':
            ev.type = EV_COMPUTE_CPU;
            break;
        case 'b':
            break;
        default: // discard all other keys
            break;
        }
        if (ev.type != EV_TYPE_NUM)
        { // new event
            queue_push(ev);
        }
        quit = is_quit();
    }

    call_termios(1); // restore terminal settings

    fprintf(stderr, "INFO: Exit keyboard thread\n");
    return NULL;
}

void *serial_thread(void *d)
{
    int fd = *(int *)d;
    uint8_t msg_buf[sizeof(message)]; // maximal buffer for all possible messages defined in messages.h
    
    uint8_t *burst_buf;
    bool burst = false;
    int burst_size = 0;

    message *msg;
    int bytes_read = 0;
    int msg_size = 0;
    fprintf(stderr, "serial1\n");

    event ev = {.source = EV_NUCLEO, .type = EV_SERIAL, .data.msg = NULL};
    bool quit = false;
    unsigned char c;
    while (serial_getc_timeout(fd, SERIAL_READ_TIMOUT_MS, &c) > 0)
    {
    }; // discard garbage
    fprintf(stderr, "serial2\n");

    while (!quit)
    {
        int r = serial_getc_timeout(fd, SERIAL_READ_TIMOUT_MS, &c);
        if (r > 0)
        { // character has been read
            if (!burst){
                msg_buf[bytes_read] = c;
            } else {
                burst_buf[bytes_read] = c;
            }
            bytes_read++;
            if (bytes_read == 1)
            {
                msg = (message *)malloc(sizeof(message));
                if (c == MSG_COMPUTE_DATA_BURST){
                    burst_size = get_burst_size();
                    msg_size = burst_size;
                    burst = true;
                    get_message_size(c, &msg_size);
                    burst_buf = (uint8_t *) malloc (msg_size);
                    burst_buf[0] = c;
                    msg->data.compute_data_burst.iters = (uint8_t*) malloc (burst_size);
                } else {
                    get_message_size(c, &msg_size);
                }
                
            }
            else if (bytes_read == msg_size)
            {
                if (!burst){
                    if (!parse_message_buf(msg_buf, msg_size, msg))
                    {
                        //fprintf(stderr, "loh\n\r");
                        msg->type = MSG_ERROR;
                    }
                } else {
                    if (!parse_message_buf(burst_buf, msg_size, msg))
                    {
                        msg->type = MSG_ERROR;
                    }
                }
                ev.data.msg = msg;
                queue_push(ev);
                bytes_read = 0;
                if (burst && burst_buf){
                    free(burst_buf);
                    burst = false;
                }
            }
        }
        else if (r == 0)
        {   //read but nothing has been received
            // TODO you may put your code here
        }
        else
        {
            error("Cannot receive data from the serial port");
            quit = true;
        }
        quit = is_quit();
    }

    return NULL;
}
