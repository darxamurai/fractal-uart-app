#include "mbed.h"
#include "messages.h"

#include <math.h>

Serial serial(SERIAL_TX, SERIAL_RX);
Ticker ticker;
DigitalOut myled (LED1);

void Tx_interrupt();
void Rx_interrupt();

#define BUF_SIZE 255
#define MESSAGE_SIZE (sizeof(message))

#define VERSION_MAJOR 1
#define VERSION_MINOR 3
#define VERSION_PATCH 0

char tx_buffer[BUF_SIZE];
char rx_buffer[BUF_SIZE];

// pointers to the circular buffers
volatile int tx_in = 0;
volatile int tx_out = 0;
volatile int rx_in = 0;
volatile int rx_out = 0;

bool send_buffer(const uint8_t* msg, unsigned int size);
bool receive_message(uint8_t *msg_buf, int size, int *len);
bool send_message(const message *msg, uint8_t *buf, int size);

int count_iter(double re, double im, double c_re, double c_im, int n);
double absolute(double re, double im);
void complex_square(double *re, double *im);

InterruptIn button_event(USER_BUTTON);
volatile bool abort_request = false;

void tick()
{
    myled = !myled;
}


void button()
{
    abort_request = true;
}

// debound
#define DEBOUNCE_TIME 0.2f
Ticker deboundTimer;
bool pressIgnore = false;
void clearIgnore(){
    pressIgnore = false;    
}


int main()
{
    message msg = {.type = MSG_STARTUP, .data.startup.message = {'k','o','l','e','s','d','a','n',' '}};
    msg_version VERSION = { .major = VERSION_MAJOR, .minor = VERSION_MINOR, .patch = VERSION_PATCH };
    
    msg_set_compute set_compute;
    msg_compute compute;
    msg_compute_data compute_data;
    msg_compute_data_burst compute_data_burst;

    serial.baud(115200);
    serial.attach(&Rx_interrupt, Serial::RxIrq); // attach interrupt handler to receive data
    serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data

    uint8_t msg_buf[MESSAGE_SIZE];
    uint8_t *burst_buf = NULL;
    int msg_len, burst_len;
    
    const float period = 0.05;

    while(serial.readable()) {
        serial.getc();
    }


    for (int i = 0; i < 5; ++i) {
        tick();
        wait(period);
        tick();
        wait(period);
    }
    
    ticker.detach();
    send_message(&msg, msg_buf, MESSAGE_SIZE);
    
    msg_len = 0;

    if (!pressIgnore){
        button_event.fall(&button);
    
        deboundTimer.attach(&clearIgnore, DEBOUNCE_TIME);
        pressIgnore = true;
    }

    bool computing = false;

    while (1) {
        if (abort_request) {
            if (computing) {  //abort computing
                msg.type = MSG_ABORT;
                send_message(&msg, msg_buf, MESSAGE_SIZE);
                computing = false;
                abort_request = false;
                ticker.detach();
                myled = 0;
            }
        }
        if (rx_in != rx_out) { // something is in the receive buffer
            if (receive_message(msg_buf, MESSAGE_SIZE, &msg_len)) {
                if (parse_message_buf(msg_buf, msg_len, &msg)) {
                    switch(msg.type) {
                        case MSG_GET_VERSION:
                            msg.type = MSG_VERSION;
                            msg.data.version  = VERSION;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_SET_BAUD_RATE:
                            serial.baud(230400);
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_ABORT:
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            computing = false;
                            abort_request = false;
                            ticker.detach();
                            myled = 0;
                            break;
                        case MSG_SET_COMPUTE:
                            set_compute.c_re = msg.data.set_compute.c_re;
                            set_compute.c_im = msg.data.set_compute.c_im;
                            set_compute.d_re = msg.data.set_compute.d_re;
                            set_compute.d_im = msg.data.set_compute.d_im;
                            set_compute.n = msg.data.set_compute.n;
                            
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_COMPUTE:
                            ticker.attach(tick, period);
                            
                            compute.cid = msg.data.compute.cid;
                            compute.re = msg.data.compute.re;
                            compute.im = msg.data.compute.im;
                            compute.n_re = msg.data.compute.n_re;
                            compute.n_im = msg.data.compute.n_im;
                            burst_len = compute.n_re;
                            
                            compute_data.i_re = compute_data.i_im = 0;
                            
                            compute_data_burst.iters = (uint8_t *) malloc (burst_len);
                            get_message_size(MSG_COMPUTE_DATA_BURST, &burst_len);
                            burst_buf = (uint8_t *) malloc (burst_len);
                            
                            computing = true;
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                    } // end switch
                } else { // message has not been parsed send error
                    msg.type = MSG_ERROR;
                    send_message(&msg, msg_buf, MESSAGE_SIZE);
                }
            } // end message received
        } else if (computing) {
            if (compute_data.i_re < compute.n_re && compute_data.i_im < compute.n_im) {
                double x = compute.re + compute_data.i_re * set_compute.d_re;
                double y = compute.im + compute_data.i_im * set_compute.d_im;
                
                compute_data_burst.iters[compute_data.i_re] = count_iter(x, y, set_compute.c_re, set_compute.c_im, set_compute.n);
                
                compute_data.i_re++;
                if (compute_data.i_re == compute.n_re){
                    compute_data.i_re = 0;
                    compute_data.i_im++;
                    
                    msg.type = MSG_COMPUTE_DATA_BURST;
                    msg.data.compute_data_burst.chunk_id = compute.cid;
                    msg.data.compute_data_burst.length = compute.n_re;
                    msg.data.compute_data_burst.iters = compute_data_burst.iters;
                    send_message(&msg, burst_buf, MESSAGE_SIZE);
                }
                
            } else { //computation done
                ticker.detach();
                myled = 0;
                msg.type = MSG_DONE;
                send_message(&msg, msg_buf, MESSAGE_SIZE);
                computing = false;
                
                free(burst_buf);
                free(compute_data_burst.iters);
            }
        } else {
            sleep(); // put the cpu to sleep mode, it will be wakeup on interrupt
        }
    } // end while (1)
}



void Tx_interrupt()
{
    // send a single byte as the interrupt is triggered on empty out buffer
    if (tx_in != tx_out) {
        serial.putc(tx_buffer[tx_out]);
        tx_out = (tx_out + 1) % BUF_SIZE;
    } else { // buffer sent out, disable Tx interrupt
        USART2->CR1 &= ~USART_CR1_TXEIE; // disable Tx interrupt
    }
    return;
}

void Rx_interrupt()
{
    // receive bytes and stop if rx_buffer is full
    while ((serial.readable()) && (((rx_in + 1) % BUF_SIZE) != rx_out)) {
        rx_buffer[rx_in] = serial.getc();
        rx_in = (rx_in + 1) % BUF_SIZE;
    }
    return;
}

bool send_buffer(const uint8_t* msg, unsigned int size)
{
    if (!msg && size == 0) {
        return false;    // size must be > 0
    }
    int i = 0;
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt on empty out buffer
    bool empty = (tx_in == tx_out);
    while ( (i == 0) || i < size ) { //end reading when message has been read
        if ( ((tx_in + 1) % BUF_SIZE) == tx_out) { // needs buffer space
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for sending buffer
            while (((tx_in + 1) % BUF_SIZE) == tx_out) {
                /// let interrupt routine empty the buffer
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        tx_buffer[tx_in] = msg[i];
        i += 1;
        tx_in = (tx_in + 1) % BUF_SIZE;
    } // send buffer has been put to tx buffer, enable Tx interrupt for sending it out
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return true;
}

bool receive_message(uint8_t *msg_buf, int size, int *len)
{
    bool ret = false;
    int i = 0;
    *len = 0; // message size
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    while ( ((i == 0) || (i != *len)) && i < size ) {
        if (rx_in == rx_out) { // wait if buffer is empty
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for receing buffer
            while (rx_in == rx_out) { // wait of next character
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        uint8_t c = rx_buffer[rx_out];
        if (i == 0) { // message type
            if (get_message_size(c, len)) { // message type recognized
                msg_buf[i++] = c;
                ret = *len <= size; // msg_buffer must be large enough
            } else {
                ret = false;
                break; // unknown message
            }
        } else {
            msg_buf[i++] = c;
        }
        rx_out = (rx_out + 1) % BUF_SIZE;
    }
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return ret;
}

bool send_message(const message *msg, uint8_t *buf, int size)
{
    int len = 0;
    return fill_message_buf(msg, buf, size, &len) && send_buffer(buf, len);
}

int count_iter(double re, double im, double c_re, double c_im, int n){
    for (int i = 1; i <= n; ++i){
        complex_square(&re, &im);
        re += c_re;
        im += c_im;
        if (absolute(re, im) >= 2){
            return i;
        }
    }
    return n;
}

double absolute(double re, double im){
    return sqrt(re*re + im*im);
}

void complex_square(double *re, double *im){
    double a = (*re)*(*re) - (*im)*(*im);
    double b = 2 * (*re) * (*im);
    
    *re = a;
    *im = b;
}