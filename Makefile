CFLAGS+= -Wall -Werror -std=gnu99 -g
LDFLAGS+= -pthread -lm

CFLAGS+=$(shell sdl2-config --cflags)
LDFLAGS+=$(shell sdl-config --libs)

#CFLAGS+=-DBAUD_RATE=691200 - for the patch1

BINARIES=fractal-main

LDFLAGS+=$(shell sdl2-config --libs)

all: ${BINARIES}

OBJS=${patsubst %.c,%.o,${wildcard *.c}}

fractal-main: ${OBJS}
	${CC} ${OBJS} ${LDFLAGS} -o $@

${OBJS}: %.o: %.c
	${CC} -c ${CFLAGS} $< -o $@

clean:
	rm -f ${BINARIES} ${OBJS}
