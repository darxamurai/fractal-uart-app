#ifndef __MAIN_H__
#define __MAIN_H__

#include "messages.h"
#include "event_queue.h"

void* main_thread(void*);
bool send_message(int fd, message *msg);

void keyboard_events(event ev, int fd);
void nucleo_events(event ev);

void set_serial(char* serial);

#endif