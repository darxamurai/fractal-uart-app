#ifndef __THREADS_H__
#define __THREADS_H__

void* keyboard_thread(void*);
void* serial_thread(void*);

void call_termios(int reset);

#endif