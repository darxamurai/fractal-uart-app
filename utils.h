#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <math.h>

void info(char* msg);

void error(char* msg);



double absolute(double re, double im);

void complex_square(double *re, double *im);



void call_termios(int reset);

void color_switch(int i, double c_re, double c_im, double re_min, double re_max, double im_min, double im_max);