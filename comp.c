#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "utils.h"
#include "comp.h"

static struct
{
    double c_re;
    double c_im;
    uint8_t n;

    double re_min;
    double re_max;
    double im_min;
    double im_max;

    double d_re;
    double d_im;

    uint8_t *grid;
    int grid_w;
    int grid_h;

    int x;
    int y;
    int burst_y;

    uint8_t cid;
    uint8_t nbr_chunks;

    double chunk_x;
    double chunk_y;

    uint8_t chunk_nx;
    uint8_t chunk_ny;

    bool computing;
    bool done;
    bool paused;
    int move;
} comp = {
    .c_re = -0.4,
    .c_im = 0.6,
    .n = 60,
    .re_min = -1.6,
    .re_max = 1.6,
    .im_min = -1.1,
    .im_max = 1.1,
    .grid = NULL,
    .grid_w = 320,
    .grid_h = 240};

void comp_init(void)
{
    comp.grid = (uint8_t *)malloc(comp.grid_h * comp.grid_w);
    comp.d_re = (comp.re_max - comp.re_min) / (1. * comp.grid_w);
    comp.d_im = -(comp.im_max - comp.im_min) / (1. * comp.grid_h);
    comp.chunk_nx = comp.grid_w / 10;
    comp.chunk_ny = comp.grid_h / 10;
    comp.nbr_chunks = (comp.grid_w * comp.grid_h) / (comp.chunk_nx * comp.chunk_ny);
}

void comp_clean(void)
{
    if (comp.grid)
    {
        free(comp.grid);
        comp.grid = NULL;
    }
}

void arg_handling(int argc, char *argv[])
{
    while (argc > 0 && argv[0][0] == '-')
    {
        int n;
        switch (argv[0][1])
        {
        case 'c':
            comp.c_re = atof(argv[1]);
            comp.c_im = atof(argv[2]);
            n = 3;
            break;

        case 'n':
            comp.n = atoi(argv[1]);
            n = 2;
            break;

        case 'm':
            comp.re_min = atof(argv[1]);
            comp.re_max = atof(argv[2]);
            comp.im_min = atof(argv[3]);
            comp.im_max = atof(argv[4]);
            n = 5;
            break;

        case 'g':
            comp.grid_w = atoi(argv[1]);
            comp.grid_h = atoi(argv[2]);
            n = 3;
            break;

        default:
            n = 0;
            break;
        }
        if (argc > n)
        {
            argv = &argv[n];
        }
        argc -= n;
    }
}

bool is_computing(void)
{
    return comp.computing;
}

bool is_done(void)
{
    return comp.done;
}

void abort_computing(void)
{
    comp.paused = true;
}

bool is_paused(void)
{
    return is_computing() ? comp.paused : true;
}

void resume_comp(void)
{
    comp.paused = false;
}

void reset_chunk(void)
{
    comp.computing = false;
    comp_clean();
    comp_init();
}

void choose_compute(){
    color_switch(0, comp.c_re, comp.c_im, comp.re_min, comp.re_max, comp.im_min, comp.im_max);
    int c, num = 0;
    hide_window();

    call_termios(0);


    double* param[] = {&(comp.c_re), &(comp.c_im), &(comp.re_min), &(comp.re_max), &(comp.im_min), &(comp.im_max)}; 

    while ((c = getchar()) != 13)
    {
        if (c == 27 && getchar() == 91){
            
            c = getchar();
            switch (c){
                case 65:
                *(param[num]) += 0.05;
                break;

                case 66:
                *(param[num]) -= 0.05;
                break;

                case 67:
                num = (num + 1) % 6;
                break;

                case 68:
                if (num == 0){
                    num = 5;
                } else {
                    num--;
                }
                break;

                default:
                break;
            }
            color_switch(num, comp.c_re, comp.c_im, comp.re_min, comp.re_max, comp.im_min, comp.im_max);
        }
    }
    fprintf(stderr,"\n\n");
    call_termios(1);
    show_window();
    
}

bool set_compute(message *msg)
{
    bool ret = !is_computing();

    if (ret)
    {
        comp.nbr_chunks = (comp.grid_w * comp.grid_h) / (comp.chunk_nx * comp.chunk_ny);

        msg->type = MSG_SET_COMPUTE;
        msg->data.set_compute.c_re = comp.c_re;
        msg->data.set_compute.c_im = comp.c_im;
        msg->data.set_compute.d_re = comp.d_re;
        msg->data.set_compute.d_im = comp.d_im;
        msg->data.set_compute.n = comp.n;
        comp.done = false;
        comp.move = 0;
    }

    return ret;
}

bool compute(message *msg)
{
    if (!is_computing())
    {
        info("computation started");
        comp.cid = 0;
        comp.computing = true;
        comp.move = 0;
        comp.x = comp.y = comp.burst_y = 0;
        comp.chunk_x = comp.re_min;
        comp.chunk_y = comp.im_max;
        msg->type = MSG_COMPUTE;
    }
    else if (is_paused())
    {
        info("computation resumed");
        resume_comp();
        comp.burst_y = 0;
        msg->type = MSG_COMPUTE;
    }
    else
    {
        comp.cid += 1;
        if (comp.cid < comp.nbr_chunks)
        {
            comp.burst_y = 0;
            comp.x += comp.chunk_nx;
            comp.chunk_x += comp.chunk_nx * comp.d_re;
            if (comp.x >= comp.grid_w)
            {
                comp.x = 0;
                comp.chunk_x = comp.re_min;

                comp.y += comp.chunk_ny;
                comp.chunk_y += comp.chunk_ny * comp.d_im;
            }
            msg->type = MSG_COMPUTE;
        }
        else
        {
        }
    }

    if (comp.computing && msg->type == MSG_COMPUTE)
    {
        msg->data.compute.cid = comp.cid;
        msg->data.compute.re = comp.chunk_x;
        msg->data.compute.im = comp.chunk_y;
        msg->data.compute.n_re = comp.chunk_nx;
        msg->data.compute.n_im = comp.chunk_ny;
    }
    return is_computing();
}

void update_grid(msg_compute_data *msg)
{
    if (msg->cid == comp.cid)
    {
        const int index = comp.x + msg->i_re + (comp.y + msg->i_im) * comp.grid_w;
        if (index >= 0 && index < (comp.grid_w * comp.grid_h))
        {
            comp.grid[index] = msg->iter;
        }
        if ((comp.cid + 1) >= comp.nbr_chunks && (msg->i_re + 1) == comp.chunk_nx && (msg->i_im + 1) == comp.chunk_ny)
        {
            comp.done = true;
            comp.computing = false;
        }
    }
    else
    {
        fprintf(stderr, "ERROR: bad chunk id\n\r");
    }
}

void update_grid_burst(msg_compute_data_burst *msg)
{
    if (msg->chunk_id == comp.cid)
    {
        const int index = comp.x + (comp.y + comp.burst_y) * comp.grid_w;
        for (int i = 0; i < msg->length; ++i)
        {
            comp.grid[index + i] = msg->iters[i];
        }
        comp.burst_y++;
        if ((comp.cid + 1) >= comp.nbr_chunks && comp.burst_y == comp.chunk_ny)
        {
            comp.done = true;
            comp.computing = false;
        }
    }
    else
    {
        fprintf(stderr, "ERROR: bad chunk id\n\r");
    }
    free(msg->iters);
}

void compute_on_comp()
{
    if (!is_computing())
    {
        comp.cid = 0;
        comp.computing = true;
        comp.move = 0;
        comp.x = comp.y = comp.burst_y = 0;
        comp.chunk_x = comp.re_min;
        comp.chunk_y = comp.im_max;
    }
    while (comp.cid < comp.nbr_chunks)
    {
        compute_chunk(comp.chunk_x, comp.chunk_y);

        comp.x += comp.chunk_nx;
        comp.chunk_x += comp.chunk_nx * comp.d_re;
        if (comp.x >= comp.grid_w)
        {
            comp.x = 0;
            comp.chunk_x = comp.re_min;

            comp.y += comp.chunk_ny;
            comp.chunk_y += comp.chunk_ny * comp.d_im;
        }
        comp.cid++;
    }
    comp.computing = false;
    comp.done = true;
}

void compute_chunk(double x, double y)
{
    for (int i = 0; i < comp.chunk_ny; ++i){
        int index = comp.x + (comp.y + i) * comp.grid_w;

        y = comp.chunk_y + i * comp.d_im;
        for (int j = 0; j < comp.chunk_nx; ++j){
            x = comp.chunk_x + j * comp.d_re;
            comp.grid[index + j] = count_iter(x, y);
        }
    }
}

int get_burst_size()
{
    return comp.chunk_nx;
}

void get_img_size(int *w, int *h)
{
    *w = comp.grid_w;
    *h = comp.grid_h;
}

void update_image(int w, int h, unsigned char *img)
{
    for (int i = 0; i < w * h; ++i)
    {
        const double t = 1. * comp.grid[i] / (comp.n + 1.0);
        *(img++) = 9 * (1 - t) * t * t * t * 255;
        *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
        *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
    }
}

void move_grid(int param)
{
    uint8_t *grid = (uint8_t *)malloc(comp.grid_h * comp.grid_w);
    int move_idx;

    comp.done = false;
    comp.move = param;

    switch (param)
    {
    case 1:
        comp.im_max -= comp.d_im * comp.chunk_ny;
        comp.im_min -= comp.d_im * comp.chunk_ny;

        move_idx = comp.chunk_ny * comp.grid_w;
        for (int i = 0; i < comp.grid_w * (comp.grid_h - comp.chunk_ny); ++i)
        {
            grid[i + move_idx] = comp.grid[i];
        }
        break;

    case 2:
        comp.im_max += comp.d_im * comp.chunk_ny;
        comp.im_min += comp.d_im * comp.chunk_ny;

        move_idx = comp.chunk_ny * comp.grid_w;
        for (int i = 0; i < comp.grid_w * (comp.grid_h - comp.chunk_ny); ++i)
        {
            grid[i] = comp.grid[i + move_idx];
        }
        break;

    case 3:
        comp.re_min -= comp.d_re * comp.chunk_nx;
        comp.re_max -= comp.d_re * comp.chunk_nx;

        move_idx = comp.chunk_nx;
        for (int i = 0; i < comp.grid_w * comp.grid_h; ++i)
        {
            if (i % comp.grid_w < comp.grid_w - comp.chunk_nx)
            {
                grid[i + move_idx] = comp.grid[i];
            }
        }
        break;

    case 4:
        comp.re_min += comp.d_re * comp.chunk_nx;
        comp.re_max += comp.d_re * comp.chunk_nx;

        move_idx = comp.chunk_nx;
        for (int i = 0; i < comp.grid_w * comp.grid_h; ++i)
        {
            if (i % comp.grid_w < comp.grid_w - comp.chunk_nx)
            {
                grid[i] = comp.grid[i + move_idx];
            }
        }
        break;

    default:
        break;
    }
    free(comp.grid);
    comp.grid = grid;
}

bool compute_move(int param, message *msg)
{
    if (!is_computing())
    {
        comp.cid = 0;
        comp.computing = true;
        comp.nbr_chunks = 10;
        comp.burst_y = 0;

        switch (param)
        {
        case 1:
        case 3:
            comp.x = comp.y = 0;
            comp.chunk_x = comp.re_min;
            comp.chunk_y = comp.im_max;
            break;

        case 2:
            comp.x = 0;
            comp.y = comp.grid_h - comp.chunk_ny;
            comp.chunk_x = comp.re_min;
            comp.chunk_y = comp.im_min - comp.d_im * comp.chunk_ny;
            break;

        case 4:
            comp.x = comp.grid_w - comp.chunk_nx;
            comp.y = 0;
            comp.chunk_x = comp.re_max - comp.d_re * comp.chunk_nx;
            comp.chunk_y = comp.im_max;
            break;

        default:
            break;
        }
        msg->type = MSG_COMPUTE;
    }
    else if (is_paused())
    {
        resume_comp();
        comp.burst_y = 0;
        msg->type = MSG_COMPUTE;
    }
    else
    {
        comp.cid += 1;
        if (comp.cid < comp.nbr_chunks)
        {
            comp.burst_y = 0;
            switch (param)
            {
            case 1:
            case 2:
                comp.x += comp.chunk_nx;
                comp.chunk_x += comp.chunk_nx * comp.d_re;
                break;
            case 3:
            case 4:
                comp.y += comp.chunk_ny;
                comp.chunk_y += comp.chunk_ny * comp.d_im;
                break;
            default:
                break;
            }

            msg->type = MSG_COMPUTE;
        }
        else
        {
        }
    }

    if (comp.computing && msg->type == MSG_COMPUTE)
    {
        msg->data.compute.cid = comp.cid;
        msg->data.compute.re = comp.chunk_x;
        msg->data.compute.im = comp.chunk_y;
        msg->data.compute.n_re = comp.chunk_nx;
        msg->data.compute.n_im = comp.chunk_ny;
    }
    return is_computing();
}

int is_move()
{
    return comp.move;
}

int count_iter(double re, double im)
{
    for (int i = 1; i <= comp.n; ++i)
    {
        complex_square(&re, &im);
        re += comp.c_re;
        im += comp.c_im;
        if (absolute(re, im) >= 2)
        {
            return i;
        }
    }
    return comp.n;
}

void zoom_out(){
    double size_re = comp.re_max - comp.re_min;
    double size_im = comp.im_max - comp.im_min;
    comp.re_min -= size_re/20;
    comp.re_max += size_re/20;
    comp.im_min -= size_im/20;
    comp.im_max += size_im/20;
}

void zoom_in(){
    double size_re = comp.re_max - comp.re_min;
    double size_im = comp.im_max - comp.im_min;
    comp.re_min += size_re/20;
    comp.re_max -= size_re/20;
    comp.im_min += size_im/20;
    comp.im_max -= size_im/20;
}