#ifndef __COMP_H__
#define __COMP_H__

#include "messages.h"
#include "xwin_sdl.h"

void comp_init(void);
void comp_clean(void);

void arg_handling(int argc, char *argv[]);

bool is_computing(void);
bool is_done(void);
bool is_paused(void);
void resume_comp(void);

void abort_computing(void);
void reset_chunk(void);

void choose_compute();
bool set_compute(message *msg);
bool compute(message *msg);

void update_grid(msg_compute_data *msg);
void update_grid_burst(msg_compute_data_burst *msg);

void compute_on_comp();
void compute_chunk(double x, double y);

int get_burst_size();

void get_img_size(int *w, int *h);
void update_image(int w, int h, unsigned char *img);

void move_grid(int param);
bool compute_move(int param, message *msg);
int is_move(void);

int count_iter(double re, double im);

void zoom_out();
void zoom_in();

#endif