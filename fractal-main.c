#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "main.h"
#include "threads.h"
#include "gui.h"
#include "comp.h"
#include "prg_serial_nonblock.h"

#ifndef BAUD_RATE
#define BAUD_RATE B115200
#endif

// - main ---------------------------------------------------------------------
int main(int argc, char *argv[])
{
    fprintf(stderr, "\n\e[1;33mWELCOME TO FRACTAL APP \e[0m\n\n");
    int fd = -1;
    char *serial;

    if (argc > 2){
        if (argv[1][0] == '/'){
            serial = argv[1];
            argc -= 2;
            argv = &argv[2];
        } else {
            serial = "/dev/ttyACM0";
            argc--;
            argv = &argv[1];
        }
        arg_handling(argc, argv);
    } else if (argc == 2){
        serial = argv[1];
    } else {
        serial = "/dev/ttyACM0";
    }
    
    set_serial(serial);
    fd = serial_open(serial, BAUD_RATE);

    if (fd == -1)
    {
        set_serial(NULL);
        fprintf(stderr, "You're in COMPUTER mode.\n\nControl keys:\n \ts \e[1;35m==\e[0m set compute\n \tc \e[1;35m==\e[0m compute on computer\n \t\u25b6 \e[1;35m==\e[0m move right\n \t\u25c0 \e[1;35m==\e[0m move left\n \t\u25b2 \e[1;35m==\e[0m move up\n \t\u25bc \e[1;35m==\e[0m move down\n \tq \e[1;35m==\e[0m quit app\n \t- \e[1;35m==\e[0m zoom out\n \t= \e[1;35m==\e[0m zoom in\n\n");
        int w, h;
        get_img_size(&w, &h);
        fprintf(stderr, "grid size: %dx%d\n\n", w, h);
    } else {
        fprintf(stderr, "You're in NUCLEO mode.\n\nControl keys:\n \ts \e[1;35m==\e[0m set compute\n \t1 \e[1;35m==\e[0m start/resume computation on nucleo\n \ta \e[1;35m==\e[0m pause computation\n \tg \e[1;35m==\e[0m get version\n \tc \e[1;35m==\e[0m compute on computer\n \t\u25b6 \e[1;35m==\e[0m move right\n \t\u25c0 \e[1;35m==\e[0m move left\n \t\u25b2 \e[1;35m==\e[0m move up\n \t\u25bc \e[1;35m==\e[0m move down\n \tq \e[1;35m==\e[0m quit app\n \t- \e[1;35m==\e[0m zoom out\n \t= \e[1;35m==\e[0m zoom in\n\n");
        int w, h;
        get_img_size(&w, &h);
        fprintf(stderr, "grid size: %dx%d\n\n", w, h);
    }

    enum
    {
        KEYBOARD,
        SERIAL,
        MAIN,
        GUI,
        NUM_THREADS
    };

    
    void *(*thr_functions[])(void *) = {keyboard_thread, serial_thread, main_thread, gui_thread};
    

    pthread_t threads[NUM_THREADS];

    for (int i = 0; i < NUM_THREADS; ++i)
    {
        pthread_create(&threads[i], NULL, thr_functions[i], &fd);
    }
fprintf(stderr, "loh2\n");

    for (int i = 0; i < NUM_THREADS; ++i)
    {
        pthread_join(threads[i], NULL);
    }
    serial_close(fd);
fprintf(stderr, "loh\n");
    fprintf(stderr, "\n\e[1;31mQUIT\e[0m\n\n");
    
    return EXIT_SUCCESS;
}
